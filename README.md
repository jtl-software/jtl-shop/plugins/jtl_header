# JTL Header

Ein Plugin zur Anpassung des Headers im Nova-Template.

Kompatibel mit JTL-Shop Version 5.1.x. 

## Dieses Plugin wird ab JTL-Shop Version 5.2 nicht mehr benötigt

Ab JTL-Shop 5.2 befinden sich im Nova-Template Einstellungen, welche die Nutzung dieses Plugins überflüssig machen. 

## Funktionsumfang
Mögliche Einstellungen:

- Menü in eigener Zeile anzeigen
- Anzeige bei vielen Menüeinträgen
- Menüausrichtung
- Anzeige beim Scrollen
- Logo Höhe (in px) (ab Shop 5.0.1)
- Suchfeld Breite (in px) (ab Shop 5.0.1)
- Suchfeld Positionierung (ab Shop 5.0.1)
- Suchfeld Abstand rechts (in px) (ab Shop 5.0.1)

## Allgemeine Hinweise

- Nutzung des Plugins nur in Verbindung mit dem vom JTL-Shop 5 ausgelieferten Nova-Template möglich.
- Das Template ersetzt die Smarty-Blocks ***layout-header-header*** und ***layout-header-nav-icons-include-header-nav-search***.
