<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 */

namespace Plugin\jtl_header;

use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;

/**
 * Class Bootstrap
 * @package Plugin\jtl_header
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritdoc
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);

        $plugin = $this->getPlugin();
        if (Shop::isFrontend()) {
            $dispatcher->listen('shop.hook.' . \HOOK_SMARTY_INC, static function (array $args) use ($plugin) {
                $config                = $plugin->getConfig();
                $smarty                = $args['smarty'];
                $menuSearchWidth       = $config->getValue('jtl_header_menu_search_width') > 0
                    ? $config->getValue('jtl_header_menu_search_width') . 'px'
                    : '100%';
                $menuLogoHeight        = $config->getValue('jtl_header_menu_logoheight') > 0
                    ? $config->getValue('jtl_header_menu_logoheight') . 'px'
                    : 0;
                $menuSearchMarginRight = $config->getValue('jtl_header_menu_search_margin_right') > 0
                    ? $config->getValue('jtl_header_menu_search_margin_right') . 'px'
                    : 0;
                /** @var JTLSmarty $smarty */
                $smarty->assign('menuSingleRow', $config->getValue('jtl_header_menu_single_row') === 'Y');
                $smarty->assign('menuMultipleRows', $config->getValue('jtl_header_menu_multiple_rows') === 'multiple');
                $smarty->assign('menuScroll', $config->getValue('jtl_header_menu_scroll') === 'menu');
                $smarty->assign('menuCentered', $config->getValue('jtl_header_menu_center') === 'center');
                $smarty->assign('menuSearchWidth', $menuSearchWidth);
                $smarty->assign('menuLogoHeight', $menuLogoHeight);
                $smarty->assign('menuSearchPosition', $config->getValue('jtl_header_menu_search_position'));
                $smarty->assign('menuSearchMarginRight', $menuSearchMarginRight);
            });
        }
    }
}
